#!/bin/bash

git clone --depth 1 https://git.jacknet.io/jackhadrill/vimrc.git
cp -r vimrc/.vim* ~/
rm -rf vimrc