#!/bin/bash -e

VM_UUID=""
VM_UUIDS="$(VBoxManage list vms)"
UUID_REGEX="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"

if [[ $# -eq 0 ]]; then
  echo "Virtual machine UUIDs:"
  echo "${VM_UUIDS}"
  echo ""
  read -p "Enter virtual machine UUID: " VM_UUID
else
  VM_UUID=$1
fi

if ! [[ $(echo "${VM_UUID}" | grep -oE "${UUID_REGEX}") == "${VM_UUID}" ]] || ! [[ "${VM_UUIDS}" =~ .*"${VM_UUID}".* ]]; then
  echo "Invalid virtual machine UUID."
  exit 1
fi

echo "Creating snapshot of original virtual machine..."
SNAPSHOT_UUID=$(VBoxManage snapshot ${VM_UUID} take $(date +%s) | grep -oE "${UUID_REGEX}")

echo "Creating new virtual machine from snapshot..."
CLONED_UUID=$(uuidgen)
VBoxManage clonevm ${VM_UUID} --snapshot ${SNAPSHOT_UUID} --register --uuid ${CLONED_UUID} --options=Link --options=keephwuuids --options=keepdisknames --options=keepallmacs

echo ""
echo "Original virtual machine UUID: ${VM_UUID}"
echo "Snapshot UUID: ${SNAPSHOT_UUID}"
echo "Cloned virtual machine UUID: ${CLONED_UUID}"

# Do stuff here!
sleep 5

echo ""
echo "Deleting cloned virtual machine..."
VBoxManage unregistervm --delete ${CLONED_UUID}
echo "Deleting snapshot..."
VBoxManage snapshot ${VM_UUID} delete ${SNAPSHOT_UUID}
