#!/bin/bash

PACKAGE=$1

apt-get update
DEPENDENCIES=$(apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances ${PACKAGE} | grep "^\w" | sort -u)

TMP="/tmp/${PACKAGE}-$(date +%s)"
mkdir -p ${TMP}

pushd ${TMP}
echo ${DEPENDENCIES} | xargs apt-get download
apt-get download ${PACKAGE}
popd

tar -cvf "${PACKAGE}.tar" -C ${TMP} .
